# Development stage

## System requirements

* Debian 10
* Bash
* Python 3.7.x
* pip
* pyenv and virtualenv plugin

### Setup virtualenv

* [Install and configure pyenv](https://github.com/pyenv/pyenv)
* [Install and configure pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv)

Creating a virtualenv:

    pyenv local 3.7.3
    pyenv virtualenv 3.7.3 hello_django

Activating a virtualenv:

    pyenv activate hello_django

Exiting from virtualenv:

    pyenv deactivate

## Install dependencies

    pip install -r requirements/base.txt
    pip install -r requirements/development.txt

## About Django

### Create Django project

    django-admin startproject hellosite

### Create Django App

    ./manage.py startapp mainapp

### Run testserver

    hellosite/manage.py runserver   # make serve

### Run tests

You can use the Django runner:

    hellosite/manage.py test polls  # make unit-test

or pytest:

    pytest hellosite    # make unit-pytest

NOTE: In order to make this task easier, instead of build and install all the
time, it can be great to create a symbolic link from django-polls/polls to
hellosite/polls:

    ln -s $(pwd)/django-polls/polls hellosite/polls

## Manage database

### Create tables from models

Create new models, then:

    ./manage.py makemigrations  # make makemigrations

To create operations that is going to be made into database and then:

    ./manage.py migrate     # make migrate

To modify database to appl changes to database.

### Create a superuser

    ./manage.py createsuperuser

## Analysis tools

* [Prospector usage](https://prospector.readthedocs.io/en/master/usage.html)
* [Radon usage](https://radon.readthedocs.io/en/latest/commandline.html)


# Build stage

## Install dependencies

For building the polls app is necesary to use wheel. So install it:

    pip install wheel

Then you can run:

    python setup bdist_wheel

but this is not necesary because we have a 'make build' command that do all this
stuff :) Take a look for at it.

# Deploy stage

First of all, copy the package built in the previous stage and put it where it
should be installed (real or virtual machine, virtual environment, etc). Then:

    pip install django-polls/dist/package.whl

where package will be whatever 'make build' produce.

For uninstalling this package you only have to type:

    pip uninstall -y django-polls

# Last words

Just take a look at makefile, there are some commands that it can be helpful.
