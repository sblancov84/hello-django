from django.contrib.admin import site, ModelAdmin, TabularInline

from polls.models import Question, Choice


class ChoiceInline(TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(ModelAdmin):
    list_display = [
        'text',
        'published',
        'was_published_recently',
    ]
    fieldsets = [
        (None, {'fields': ['text']}),
        ('Date Information', {'fields': ['published']}),
    ]
    inlines = [ChoiceInline]
    list_filter = ['published']
    search_fields = ['text']


site.register(Question, QuestionAdmin)
site.register(Choice)
