import datetime

from django.utils import timezone
from django.db.models import (
    Model, IntegerField, CharField, DateTimeField, ForeignKey, CASCADE)


class Question(Model):
    text = CharField("description", max_length=200)
    published = DateTimeField("published date")

    def __str__(self):
        return self.text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.published <= now

    was_published_recently.admin_order_field = 'published'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'


class Choice(Model):
    question = ForeignKey(Question, on_delete=CASCADE)
    text = CharField(max_length=200)
    votes = IntegerField(default=0)

    def __str__(self):
        return self.text
