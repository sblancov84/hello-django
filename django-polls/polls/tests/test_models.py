import datetime

from django.utils import timezone
from django.test import TestCase

from polls.models import Question


class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(published=time)
        actual = future_question.was_published_recently()
        expected = False
        self.assertIs(actual, expected)

    def test_was_published_recently_with_recent_question(self):
        day = datetime.timedelta(hours=23, minutes=59, seconds=59)
        time = timezone.now() - day
        recent_question = Question(published=time)
        self.assertIs(recent_question.was_published_recently(), True)

    def test_was_published_recently_with_old_question(self):
        now = timezone.now()
        day = datetime.timedelta(days=1, seconds=1)
        time = now - day
        old_question = Question(published=time)
        self.assertIs(old_question.was_published_recently(), False)
