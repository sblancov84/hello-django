from django.urls import reverse
from django.test import TestCase

from polls.tests.views.utils import create_question


class QuestionDetailViewTests(TestCase):

    def test_future_question(self):
        future_question = create_question(text='Future question.', days=5)
        url = reverse('polls:detail', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        past_question = create_question(text='Past Question.', days=-5)
        url = reverse('polls:detail', args=(past_question.pk,))
        response = self.client.get(url)
        self.assertContains(response, 'Past Question.')
