from django.urls import reverse
from django.test import TestCase

from polls.tests.views.utils import create_question


class QuestionIndexViewTest(TestCase):

    def setUp(self):
        self.url = reverse('polls:index')

    def test_no_questions_ok(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_no_questions_content_is_rigth(self):
        response = self.client.get(self.url)
        # # response.content is binary
        # text = response.content.decode('utf-8')
        # self.assertTrue('No polls are available' in text)
        self.assertContains(response, 'No polls are available')

    def test_past_question(self):
        create_question(text="Past question.", days=-30)
        response = self.client.get(self.url)
        # actual = response.context['latest_question_list'].first().text
        # expected = "Past question."
        # self.assertEqual(actual, expected)
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Past question.>']
        )

    def test_future_question(self):
        create_question(text="Future question.", days=30)
        response = self.client.get(self.url)
        self.assertContains(response, "No polls are available.")

    def test_past_and_future_question(self):
        create_question(text="Past question.", days=-30)
        create_question(text="Future question.", days=30)
        response = self.client.get(self.url)
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Past question.>']
        )

    def test_two_past_question(self):
        create_question(text="Past question 1.", days=-30)
        create_question(text="Past question 2.", days=-5)
        response = self.client.get(self.url)
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Past question 2.>', '<Question: Past question 1.>']
        )
