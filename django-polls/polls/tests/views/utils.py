import datetime

from django.utils import timezone

from polls.models import Question


def create_question(text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(text=text, published=time)
