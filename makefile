include .dev_env_vars

DJANGO_COMMAND = python hellosite/manage.py

base-install:
	pip install -r requirements/base.txt

develop-install:
	pip install -r requirements/develop.txt

build-install:
	pip install -r requirements/build.txt


makemigrations:
	${DJANGO_COMMAND} makemigrations

migrate:
	${DJANGO_COMMAND} migrate


analisys:
	prospector

radon-cc:
	radon cc .

radon-mi:
	radon mi -s .

radon-raw:
	radon raw -s .

radon-hal:
	radon hal .

serve:
	${DJANGO_COMMAND} runserver


unit-test:
	${DJANGO_COMMAND} test polls

unit-pytest:
	pytest hellosite


build:
	scripts/package.sh

clean:
	rm -r django-polls/dist
	rm -r django-polls/build
	rm -r django-polls/django_polls.egg-info


install-polls:
	pip install $(wildcard django-polls/dist/django_polls*)

uninstall-polls:
	pip uninstall -y django-polls
