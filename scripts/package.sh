#!/usr/bin/env bash

BASE_DIR=$(dirname $0)
TARGET_DIR=$BASE_DIR/../django-polls

pushd $TARGET_DIR
python setup.py bdist_wheel
popd

printf "\n\nYour package should be in 'django-polls/dist/'\n"
printf "\n\nYou can install this package with 'pip install django-polls/dist/package.whl'\n"
printf "\n\nYou can uninstall this package with 'pip uninstall django-polls'\n"
